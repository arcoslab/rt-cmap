# Real-time capability maps (RT-cmap)

This is a tool for creating Capability maps in real time. These maps where
introduced in [this paper](https://ieeexplore.ieee.org/document/4399105).

The code presented in this repository is related to a paper [we
published](https://dialnet.unirioja.es/servlet/articulo?codigo=8827981):

Vaglio, D. G., & Ugalde, F. R. (2022). GPU based approach for fast generation of robot capability representations. Tecnología en Marcha, 35(8), 50-57.



## Installation

First you need to install the nvidia drivers and the CUDA-SDK. If you are using
archlinux it is just the command below. If you are using debian or ubuntu is a
bit more complicated than just this.

``` bash
sudo pacman -S nvidia cuda
```

Once these dependencies are installed just install the project with poetry

``` bash
poetry install
```

## Testing

As of this moment testing has to be done manually because we do not have gitlab
runners with nvidia gpus available.

``` bash
poetry run pytest tests/
```

