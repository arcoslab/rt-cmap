# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


from numba import cuda


@cuda.jit(device=True)
def _angle_between_rotations(left, right):
    """
    calculate the angle of a rotation matrix with respect to the other
    """
    # calculate the rotation of one in the frame of the other by multiplying by
    # the inverse. But transpose is inverse in rotations. and we only need the
    # trace of the result, so I only calculate those positions

    tr0 = (left[0, 0] * right[0, 0] + left[0, 1] * right[0, 1] +
           left[0, 2] * right[0, 2])
    tr1 = (left[1, 0] * right[1, 0] + left[1, 1] * right[1, 1] +
           left[1, 2] * right[1, 2])
    tr2 = (left[2, 0] * right[2, 0] + left[2, 1] * right[2, 1] +
           left[2, 2] * right[2, 2])
    # The angle is the arccos of the trace, but because arccos is monotonous
    # and very slow, I am just using the trace like this
    return 1.0-((tr0 + tr1 + tr2) - 1.0)/2.0


@cuda.jit(device=True)
def _find_closest_orientation(orientations, pose_rot, sphere_num, roll_num):
    # We need the Z axis of multiplying the rotation by (1, 0, 0).
    # But that is just m[2, 0]
    z = pose_rot[2, 0]
    result = 0
    min_angle = _angle_between_rotations(orientations[0], pose_rot)
    n = int(sphere_num*(-z+1)/2)
    for idx in range(n*roll_num, (n+1)*roll_num):
        angle = _angle_between_rotations(orientations[idx], pose_rot)

        if angle < min_angle:
            result = idx
            min_angle = angle

    return result


@cuda.jit(device=True)
def _find_closest_position(x_bounds, y_bounds, z_bounds, xyz_step,
                           cartesian_voxelization_num, pose_vec):
    # position = step * index + low
    # index = (position - low) / step
    x_index = round((pose_vec[0] - x_bounds[0])/xyz_step[0])
    y_index = round((pose_vec[1] - y_bounds[0])/xyz_step[1])
    z_index = round((pose_vec[2] - z_bounds[0])/xyz_step[2])

    result = z_index +\
        y_index * cartesian_voxelization_num +\
        x_index * cartesian_voxelization_num**2

    return result


@cuda.jit(device=True)
def _compute_meta_coordinate(x_bounds, y_bounds, z_bounds, xyz_step,
                             cartesian_voxelization_num, sphere_num, roll_num,
                             orientations, pose_rot, pose_vec,
                             coordinate):
    coordinate[0] = _find_closest_position(
        x_bounds, y_bounds, z_bounds, xyz_step, cartesian_voxelization_num,
        pose_vec)
    coordinate[1] = _find_closest_orientation(
        orientations, pose_rot, sphere_num, roll_num)
