# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from numba import cuda
from math import cos, sin

from numba.cuda.random import xoroshiro128p_uniform_float32


@cuda.jit(device=True)
def _matrix_from_angle(angle, matrix):
    """
    Create a 3x3 Z rotation matrix from a rotation angle
    """
    matrix[0, 0] = cos(angle)
    matrix[0, 1] = -sin(angle)
    matrix[0, 2] = 0.0
    matrix[1, 0] = sin(angle)
    matrix[1, 1] = cos(angle)
    matrix[1, 2] = 0.0
    matrix[2, 0] = 0.0
    matrix[2, 1] = 0.0
    matrix[2, 2] = 1.0


@cuda.jit(device=True)
def _vector_sum(left, right, result):
    """
    Add two vectors. This has to be done because numba
    CUDA does not supports vector summation
    """
    r0 = left[0] + right[0]
    result[0] = r0
    r1 = left[1] + right[1]
    result[1] = r1
    r2 = left[2] + right[2]
    result[2] = r2


@cuda.jit(device=True)
def _vector_mul(matrix, vector, result):
    """
    Multiply a 3x3 matrix by a 3D vector. This has to be done
    because numba CUDA does not supports matrix multiplication
    """
    # This code was computer-generated
    # store the result in a temporal container because we do not want to modify
    # the result in-place
    tmp0 = (matrix[0, 0] * vector[0] + matrix[0, 1] * vector[1] +
            matrix[0, 2] * vector[2])
    tmp1 = (matrix[1, 0] * vector[0] + matrix[1, 1] * vector[1] +
            matrix[1, 2] * vector[2])
    tmp2 = (matrix[2, 0] * vector[0] + matrix[2, 1] * vector[1] +
            matrix[2, 2] * vector[2])
    result[0] = tmp0
    result[1] = tmp1
    result[2] = tmp2


@cuda.jit(device=True)
def _matrix_mul(left, right, result):
    """
    Multiply a 3x3 matrix by a 3x3 matrix. This has to be done
    because numba CUDA does not supports matrix multiplication
    """
    # This code was computer-generated
    # store the result in a temporal container because we do not want to modify
    # the result in-place
    tmp00 = (left[0, 0] * right[0, 0] + left[0, 1] * right[1, 0] +
             left[0, 2] * right[2, 0])
    tmp01 = (left[0, 0] * right[0, 1] + left[0, 1] * right[1, 1] +
             left[0, 2] * right[2, 1])
    tmp02 = (left[0, 0] * right[0, 2] + left[0, 1] * right[1, 2] +
             left[0, 2] * right[2, 2])
    tmp10 = (left[1, 0] * right[0, 0] + left[1, 1] * right[1, 0] +
             left[1, 2] * right[2, 0])
    tmp11 = (left[1, 0] * right[0, 1] + left[1, 1] * right[1, 1] +
             left[1, 2] * right[2, 1])
    tmp12 = (left[1, 0] * right[0, 2] + left[1, 1] * right[1, 2] +
             left[1, 2] * right[2, 2])
    tmp20 = (left[2, 0] * right[0, 0] + left[2, 1] * right[1, 0] +
             left[2, 2] * right[2, 0])
    tmp21 = (left[2, 0] * right[0, 1] + left[2, 1] * right[1, 1] +
             left[2, 2] * right[2, 1])
    tmp22 = (left[2, 0] * right[0, 2] + left[2, 1] * right[1, 2] +
             left[2, 2] * right[2, 2])

    result[0, 0] = tmp00
    result[0, 1] = tmp01
    result[0, 2] = tmp02
    result[1, 0] = tmp10
    result[1, 1] = tmp11
    result[1, 2] = tmp12
    result[2, 0] = tmp20
    result[2, 1] = tmp21
    result[2, 2] = tmp22


@cuda.jit(device=True)
def _pose_mul(
        left_rot, left_vec, right_rot, right_vec, result_rot, result_vec):
    """
    Multiply a pose by another
    """
    result_vec[0] = (left_rot[0, 0] * right_vec[0] +
                     left_rot[0, 1] * right_vec[1] +
                     left_rot[0, 2] * right_vec[2] +
                     left_vec[0])
    result_vec[1] = (left_rot[1, 0] * right_vec[0] +
                     left_rot[1, 1] * right_vec[1] +
                     left_rot[1, 2] * right_vec[2] +
                     left_vec[1])
    result_vec[2] = (left_rot[2, 0] * right_vec[0] +
                     left_rot[2, 1] * right_vec[1] +
                     left_rot[2, 2] * right_vec[2] +
                     left_vec[2])
    _matrix_mul(left_rot, right_rot, result_rot)


@cuda.jit(device=True)
def _calculate_pose(
        rnd_states, link_limits, link_rot, link_vec, segment_rot, segment_vec,
        result_rot, result_vec):
    result_rot[0, 0] = 1
    result_rot[0, 1] = 0
    result_rot[0, 2] = 0
    result_rot[1, 0] = 0
    result_rot[1, 1] = 1
    result_rot[1, 2] = 0
    result_rot[2, 0] = 0
    result_rot[2, 1] = 0
    result_rot[2, 2] = 1
    result_vec[0] = 0
    result_vec[1] = 0
    result_vec[2] = 0
    for idx in range(link_limits.shape[0]):
        # Calculate the joint rotation matrix
        rnd = xoroshiro128p_uniform_float32(rnd_states, cuda.grid(1))
        angle = link_limits[idx, 0] + rnd *\
            (link_limits[idx, 1] - link_limits[idx, 0])
        _matrix_from_angle(angle, segment_rot)
        segment_vec[0] = 0
        segment_vec[1] = 0
        segment_vec[2] = 0
        # Calculate the segment local transformation
        _pose_mul(
            segment_rot, segment_vec, link_rot[idx], link_vec[idx],
            segment_rot, segment_vec)
        # Calculate the global transformation
        _pose_mul(
            result_rot, result_vec,
            segment_rot, segment_vec,
            result_rot, result_vec)
