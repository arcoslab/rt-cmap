# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


from numpy import array, identity, dot, pi
from scipy.spatial.transform import Rotation


def _rotz(angle):
    result = identity(4)
    result[:3, :3] = Rotation.from_rotvec([0, 0, angle]).as_matrix()
    return result


def _roty(angle):
    result = identity(4)
    result[:3, :3] = Rotation.from_rotvec([0, angle, 0]).as_matrix()
    return result


def _rotx(angle):
    result = identity(4)
    result[:3, :3] = Rotation.from_rotvec([angle, 0, 0]).as_matrix()
    return result


def _vector(x, y, z):
    result = identity(4)
    result[:3, 3] = x, y, z
    return result


def _frame(rot, trans):
    return dot(trans, rot)


KUKA_LINKS = array([
    # 1st link
    _frame(_rotx(-pi/2), _vector(0, 0, 0.2)),
    # 2nd link
    _frame(_rotx(pi/2), _vector(0, -0.2, 0)),
    # 3rd link
    _frame(_rotx(pi/2), _vector(0, 0, 0.2)),
    # 4th link
    _frame(_rotx(-pi/2), _vector(0, 0.2, 0)),
    # 5th link
    _frame(_rotx(-pi/2), _vector(0, 0, 0.19)),
    # 6th link
    _frame(_rotx(pi/2), _vector(0, -0.078, 0)),
    # 7th link
    _frame(_rotx(0), _vector(0.0, 0.0, 0.05))
])

KUKA_LIMITS = array([
    # 1st joint
    [-169.5*pi/180, 169.5*pi/180],
    # 2nd joint
    [-119.5*pi/180, 119.5*pi/180],
    # 3rd joint
    [-169.5*pi/180, 169.5*pi/180],
    # 4th joint
    [-119.5*pi/180, 119.5*pi/180],
    # 5th joint
    [-169.5*pi/180, 169.5*pi/180],
    # 6th joint
    [-119.5*pi/180, 119.5*pi/180],
    # 7th joint
    [-169.5*pi/180, 169.5*pi/180]
])
