# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import numpy as np
from time import time
from numba import cuda
from numba.cuda.random import create_xoroshiro128p_states

from rt_cmap.utils import _find_factors, _print_progress
from rt_cmap.kuka_description import KUKA_LINKS, KUKA_LIMITS
from rt_cmap.generator import _calculate_pose
from rt_cmap.comparator import _compute_meta_coordinate
from rt_cmap.scores import _calculate_scores, _compute_colors
from rt_cmap.voxelization import generate_cmap_positions, spiral_euler


@cuda.jit
def _reachability_kernel(
        rnd_states, link_limits, link_rots, link_vecs,
        segment_rots, segment_vecs, pose_rots, pose_vecs,
        x_bounds, y_bounds, z_bounds, xyz_step,
        cartesian_voxelization_num, sphere_num, roll_num,
        orientations, coordinates, reachability_matrix, num_batches):
    pos = cuda.grid(1)
    if pos < rnd_states.shape[0] and\
       pos < pose_rots.shape[0] and pos < pose_vecs.shape[0] and\
       pos < segment_rots.shape[0] and pos < segment_vecs.shape[0] and\
       pos < coordinates.shape[0]:
        _calculate_pose(rnd_states, link_limits,
                        link_rots, link_vecs,
                        segment_rots[pos], segment_vecs[pos],
                        pose_rots[pos], pose_vecs[pos])
        _compute_meta_coordinate(
            x_bounds, y_bounds, z_bounds, xyz_step,
            cartesian_voxelization_num, sphere_num, roll_num,
            orientations, pose_rots[pos], pose_vecs[pos],
            coordinates[pos])

        position = coordinates[pos, 0]
        orientation = coordinates[pos, 1]
        reachability_matrix[position, orientation] = 1


def create_cmap(x_bounds, y_bounds, z_bounds, xyz_step,
                cartesian_voxelization_num, sphere_num, roll_num,
                cmap_positions, cmap_orientations,
                arm_length=1.2, threadsperblock=1024,
                blockspergrid=1024, arm_links=KUKA_LINKS,
                arm_limits=KUKA_LIMITS):
    """
    Create a capability map from a given voxelization

    :param x_bounds: limits in the x dimension
    :param y_bounds: limits in the y dimension
    :param z_bounds: limits in the z dimension
    :param xyz_step: cartesian step
    :param cartesian_voxelization_num: cartesian division in each dimension
    :param sphere_num: yaw-pitch divisions
    :param roll_num: roll divisions
    :param arm_length: Length of the robotic arm
    :param threadsperblock: How many threads per block
    :param blockspergrid: How many blocks per grid
    :param arm_links: Kinematic description of the arm links
    :param arm_limits: limits of each joint

    :return: capability map points positions and colors
    """

    num = threadsperblock * blockspergrid
    num_batches = round(
        (60*cartesian_voxelization_num**3*sphere_num*roll_num) /
        (50**4*10))
    scores_threads, scores_blocks = _find_factors(num)

    # Define a lot of random numbers from which the joint angles
    # will be computed
    start = time()
    rnd_states = create_xoroshiro128p_states(
        num, np.random.randint(2**16))
    print("Prepared random states in {}s".format(time()-start))

    start = time()
    # Define the link transforms
    link_rots = cuda.to_device(np.array(arm_links[:, :3, :3]))
    link_vecs = cuda.to_device(np.array(arm_links[:, :3, 3]))

    # Define the link limits
    link_limits = cuda.to_device(arm_limits)

    # Define the matrices where we will store the segment tf
    segment_rots = cuda.device_array((num, 3, 3))
    segment_vecs = cuda.device_array((num, 3))

    # Define the matrices where we will store the result tf
    pose_rots = cuda.device_array((num, 3, 3))
    pose_vecs = cuda.device_array((num, 3))

    coordinates = cuda.device_array((num, 2), dtype=np.int32)

    orientations = cuda.to_device(cmap_orientations)

    reachability_matrix = cuda.to_device(np.zeros(
        (cartesian_voxelization_num**3, cmap_orientations.shape[0]),
        dtype=np.int32))

    scores = np.empty(cartesian_voxelization_num**3)
    print("Allocated resources in {}s".format(time()-start))
    print("Generating poses ...")
    start = time()
    for batch in range(num_batches):
        _print_progress(batch, num_batches)
        _reachability_kernel[blockspergrid, threadsperblock](
            rnd_states, link_limits, link_rots, link_vecs,
            segment_rots, segment_vecs, pose_rots, pose_vecs,
            x_bounds, y_bounds, z_bounds, xyz_step,
            cartesian_voxelization_num, sphere_num, roll_num,
            orientations, coordinates, reachability_matrix, batch
        )
    print("Got Reachability Map in {}s".format(time()-start))

    start = time()
    _calculate_scores[scores_blocks, scores_threads](
        reachability_matrix, scores)

    print("Got Capability Map in {}s".format(time()-start))
    start = time()
    colors, positions = _compute_colors(
        scores, cmap_positions, cartesian_voxelization_num)
    print("Got map colors in {}s".format(time()-start))

    return (positions, colors)


def generate_cmap(
        cartesian_voxelization_num=50, sphere_num=50,
        roll_num=10, arm_links=KUKA_LINKS,
        arm_limits=KUKA_LIMITS):
    """
    Generate a capability map
    :param cartesian_voxelization_num: number fo division of space
    :param sphere_num: number of yaw/pitch divisions on sphere
    :param roll_num: number of roll angles
    :param arm_links: Kinematic description of the arm links
    :param arm_limits: limits of each joint

    :return: capability map points positions and colors
    """

    arm_length = 1.05 * sum(map(np.linalg.norm, arm_links[:, :3, 3]))

    start = time()
    middle = time()
    cmap_positions, xyz_step = generate_cmap_positions(
        -arm_length, arm_length, -arm_length,
        arm_length, -arm_length, arm_length,
        cartesian_voxelization_num)
    print("Cartesian voxelization in {}s".format(time()-middle))

    middle = time()
    cmap_orientations = spiral_euler(sphere_num, roll_num)
    print("Orientations generated in {}s".format(time()-middle))

    x_bounds = np.array((-arm_length, arm_length))
    y_bounds = np.array((-arm_length, arm_length))
    z_bounds = np.array((-arm_length, arm_length))

    positions, colors = create_cmap(
        x_bounds, y_bounds, z_bounds, np.array(xyz_step),
        cartesian_voxelization_num, sphere_num, roll_num,
        cmap_positions, cmap_orientations, arm_length=arm_length,
        arm_limits=arm_limits, arm_links=arm_links)

    end = time()
    print("Elapsed time: ", end-start)
    return (positions, colors)
