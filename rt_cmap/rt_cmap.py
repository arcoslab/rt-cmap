# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import argparse
import timeit
from rt_cmap.reachability_map import generate_cmap
from rt_cmap.visualization import vis_cmap, store_cmap


def main():
    parser = argparse.ArgumentParser(
        description="Visualize Capability Maps")

    parser.add_argument(
        "-s", "--stl", dest="arm_stl_path", default=None, type=str,
        help="Path to the STL of the arm")

    parser.add_argument(
        "-t", "--timing", dest="timing", action="store_true",
        default=False,
        help="Run many times cmap generation to get timing"
    )

    parser.add_argument(
        "-v", "--no-visualization", dest="no_visualization",
        action="store_true", default=False,
        help="Do not render 3D view"
    )

    parser.add_argument(
        "-c", "--cartesian_num", dest="cartesian_num", type=int, nargs="*",
        default=[50], help="""
        How many sundivisions for cartesian voxelization. Use multiple values
        for timing analysis.
        """
    )

    parser.add_argument(
        "-n", "--sphere_num", dest="sphere_num", type=int, nargs="*",
        default=[50], help="""
        How many sphere subdivisions. Use multiple values for timing
        analysis"""
    )

    parser.add_argument(
        "-r", "--roll-num", dest="roll_num", type=int, nargs="*",
        default=[10], help="""How many roll subdivisions. Use multiple
        values for timing analysis"""
    )

    parser.add_argument(
        "-o",  "--output", dest="output_name", type=str, default="",
        help="Output CMAP to a file"
    )

    args = parser.parse_args()
    if args.timing:
        # Generate a cmap for compilation
        generate_cmap(
            cartesian_voxelization_num=args.cartesian_num[0],
            sphere_num=args.sphere_num[0],
            roll_num=args.roll_num[0])
        elapsed_time = []
        for cartesian_num in args.cartesian_num:
            for sphere_num in args.sphere_num:
                for roll_num in args.roll_num:
                    elapsed_time.append(timeit.timeit(
                        lambda:
                        generate_cmap(
                            cartesian_voxelization_num=cartesian_num,
                            sphere_num=sphere_num,
                            roll_num=roll_num),
                        number=1))
        print("-\n"*10)
        print("Cartesian nums: ", args.cartesian_num)
        print("Sphere nums: ", args.sphere_num)
        print("Roll nums: ", args.roll_num)
        print("Elapsed times: ", elapsed_time)

    if not args.no_visualization:
        vis_cmap(
            cartesian_voxelization_num=args.cartesian_num[0],
            sphere_num=args.sphere_num[0], roll_num=args.roll_num[0],
            arm_stl_path=args.arm_stl_path)

    if args.output_name != "":
        store_cmap(cartesian_voxelization_num=args.cartesian_num[0],
                   sphere_num=args.sphere_num[0], roll_num=args.roll_num[0],
                   file_name=args.output_name)
