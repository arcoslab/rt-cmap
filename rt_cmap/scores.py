# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import numba
from numba import cuda


@cuda.jit
def _calculate_scores(reachability_matrix, scores):
    """
    Compute the scores of the capability map from the reachability matrix
    by summing each row
    """
    pos = cuda.grid(1)
    size = cuda.blockDim.x * cuda.gridDim.x
    nx = 0
    while pos+nx*size < scores.shape[0]:
        index = pos + nx*size
        nx += 1
        scores[index] = 0
        for idx in range(reachability_matrix.shape[1]):
            scores[index] += reachability_matrix[index, idx]


@numba.jit
def _compute_colors(scores, cmap_positions,
                    cartesian_voxelization_num):
    """
    Generate a score for each position
    """
    max_score = scores.max()

    # We only care about the nonzero elements
    indexes = scores.nonzero()[0]
    colors = np.zeros((3, len(indexes)))
    positions = np.zeros((3, len(indexes)))

    for idx in range(len(indexes)):
        normalized = scores[indexes[idx]] / max_score
        positions[:, idx] = cmap_positions[indexes[idx]]
        if normalized < 0.25:
            # red
            colors[0, idx] = 1
            # green
            colors[1, idx] = 4 * normalized
            # blue
            colors[2, idx] = 0
        elif normalized < 0.5:
            colors[0, idx] = 2 - 4*normalized
            colors[1, idx] = 1
            colors[2, idx] = 0
        elif normalized < 0.75:
            colors[0, idx] = 0
            colors[1, idx] = 1
            colors[2, idx] = -2 + 4*normalized
        else:
            colors[0, idx] = 0
            colors[1, idx] = 4 - 4*normalized
            colors[2, idx] = 1

    return (colors, positions)
