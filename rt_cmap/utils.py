# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import numpy as np


def _find_factors(num):
    """
    Find the factorization of the number such that they fit into
    1024
    """

    if num > 1024*1024:
        return (1024, 1024)
    for n in range(1024):
        factor = 1024-n
        if (num % factor) == 0:
            return (factor, int(num/factor))


def _print_progress(current, total):
    if int(total*current) % int(np.sqrt(total)) == 0:
        print("{}%".format(int(100*current/total)))
