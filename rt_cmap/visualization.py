# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import time
import pickle
import meshcat
import numpy as np
from rt_cmap.reachability_map import generate_cmap


def _progresive_fill(positions, arm_length, cartesian_voxelization_num):
    step = arm_length / cartesian_voxelization_num
    noise = (np.random.random(positions.shape) * 2 * step) - step/2
    return positions + noise


def _filter_cloud_by_plane(positions, colors, plane_position, plane_normal):
    worthy = np.empty(positions.shape[1])

    # if the dot product is too small use another axis to compare
    normal_to_x = (abs(np.dot(plane_normal, np.array([1, 0, 0]))) < 0.0001)

    for idx in range(len(worthy)):
        to_origin = positions[:, idx] - plane_position
        if normal_to_x:
            z = (-(to_origin[0] * plane_normal[0] +
                   to_origin[1] * plane_normal[1]) /
                 plane_normal[2])
            if z > to_origin[2]:
                worthy[idx] = 1
            else:
                worthy[idx] = 0

        else:
            x = (-(to_origin[1] * plane_normal[1] +
                   to_origin[2] * plane_normal[2]) /
                 plane_normal[0])
            if x > to_origin[0]:
                worthy[idx] = 1
            else:
                worthy[idx] = 0

    coordinates = worthy.nonzero()[0]

    return (positions[:, coordinates], colors[:, coordinates])


def vis_cmap(cartesian_voxelization_num=50,
             sphere_num=50, roll_num=10, arm_stl_path=None):
    """
    Visualize a capability map in meshcat

    :param cartesian_voxelization_num: number fo division of space
    :param sphere_num: number of yaw/pitch divisions on sphere
    :param roll_num: number of roll angles
    :param arm_stl_path: Path to the stl of a robotic arm to include in
                         visualization.
    """
    vis = meshcat.Visualizer()

    print((("="*20)+"\n")*3)

    if arm_stl_path is not None:
        arm = meshcat.geometry.StlMeshGeometry.from_file(arm_stl_path)
        vis["arm"].set_object(arm)

    positions, colors = generate_cmap(
        cartesian_voxelization_num=cartesian_voxelization_num,
        sphere_num=sphere_num, roll_num=roll_num)

    positions, colors = _filter_cloud_by_plane(
        positions, colors, np.array([0, 0, 0]), np.array([1, 0, 0]))

    visualization_positions = positions[:]  # copy no reference

    print("Filtered cloud")

    # Wait some time to be able to open the visualization
    for ids in range(100):
        arm_length = 1.15

        points = meshcat.geometry.PointCloud(
            visualization_positions, color=colors)

        vis["points/" + str(ids)].set_object(points)

        visualization_positions = _progresive_fill(
            positions, arm_length,
            cartesian_voxelization_num)

    for ids in range(30):
        time.sleep(1)


def store_cmap(cartesian_voxelization_num=50,
               sphere_num=50, roll_num=10,
               arm_stl_path=None, file_name="output.pickle"):
    """
    Store the data of the capability map

    :param cartesian_voxelization_num: number fo division of space
    :param sphere_num: number of yaw/pitch divisions on sphere
    :param roll_num: number of roll angles
    """
    positions, colors = generate_cmap(
        cartesian_voxelization_num=cartesian_voxelization_num,
        sphere_num=sphere_num, roll_num=roll_num)

    positions, colors = _filter_cloud_by_plane(
        positions, colors, np.array([0, 0, 0]), np.array([1, 0, 0]))

    with open(file_name, "wb") as out_file:
        pickle.dump([positions, colors], out_file)
