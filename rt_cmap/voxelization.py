# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from scipy.constants import golden_ratio
from scipy.spatial.transform import Rotation


def generate_cmap_positions(x_low, x_high, y_low, y_high, z_low, z_high,
                            resolution_num):
    """
    Voxelize the cartesian subspace defined by the bounds and resolution

    :param x_low: low x limit
    :param x_high: upper x limit
    :param y_low: low y limit
    :param y_high: upper y limit
    :param z_low: low z limit
    :param z_high: upper z limit
    :param resolution_num: divisions num in each dimension

    :returns: List of positions with the center of the voxels
    """
    xs, xstep = np.linspace(x_low, x_high, num=resolution_num, retstep=True)
    ys, ystep = np.linspace(y_low, y_high, num=resolution_num, retstep=True)
    zs, zstep = np.linspace(z_low, z_high, num=resolution_num, retstep=True)

    result = np.array([np.array([x, y, z])
                       for x in xs for y in ys for z in zs])

    return (result, (xstep, ystep, zstep))


def spiral_euler(num_points, num_roll):
    """
    Generate equidistant rotations using a Fibonacci spiral.

    :param num_points: Yaw-pitch generation number
    :param num_roll: Roll generation number

    :returns: list of rotation matrices
    """
    golden_angle = (2.0 - golden_ratio) * (2.0*np.pi)
    rolls = np.linspace(-np.pi, np.pi, endpoint=False, num=num_roll)
    rotations = np.empty((num_points*num_roll, 3, 3))
    for idx in range(num_points):
        pitch = np.arcsin(-1.0 + 2.0 * idx / (num_points))
        yaw = golden_angle * idx
        for idr in range(num_roll):
            rotations[idx*num_roll + idr] = Rotation.from_euler(
                "ZYX", (yaw, pitch, rolls[idr])).as_matrix()

    return rotations
