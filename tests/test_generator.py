# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 Autonomous Robots and Cognitive Systems Laboratory
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
from numba import cuda

from rt_cmap.generator import (_matrix_from_angle,
                               _vector_sum, _vector_mul,
                               _matrix_mul, _pose_mul)


def is_matrix_similar(sec, par, eps=0.0000001):
    assert np.sum(np.abs(sec - par)) < eps


@cuda.jit
def matrix_from_angle_kernel(angles, matrices):
    pos = cuda.grid(1)
    _matrix_from_angle(angles[pos], matrices[pos])


def test_matrix_from_angle():
    threadsperblock = 1024
    blockspergrid = 512
    num = threadsperblock * blockspergrid

    angles = np.linspace(-2*np.pi, 2*np.pi, num)
    p_mat = np.zeros((num, 3, 3))
    s_mat = np.zeros((num, 3, 3))
    matrix_from_angle_kernel[blockspergrid, threadsperblock](
        angles, p_mat)

    for idx, angle in enumerate(angles):
        s_mat[idx] = np.identity(3)
        s_mat[idx, 0, 0] = np.cos(angle)
        s_mat[idx, 0, 1] = -np.sin(angle)
        s_mat[idx, 1, 0] = np.sin(angle)
        s_mat[idx, 1, 1] = np.cos(angle)

    is_matrix_similar(s_mat, p_mat)


@cuda.jit
def vector_sum_kernel(lefts, rights, results):
    pos = cuda.grid(1)
    _vector_sum(lefts[pos], rights[pos], results[pos])


def test_vector_sum():
    threadsperblock = 1024
    blockspergrid = 512
    num = threadsperblock * blockspergrid

    lefts = np.random.rand(num, 3)
    rights = np.random.rand(num, 3)
    p_results = np.zeros((num, 3))

    vector_sum_kernel[blockspergrid, threadsperblock](
        lefts, rights, p_results)

    s_results = lefts + rights

    is_matrix_similar(s_results, p_results)


@cuda.jit
def vector_mul_kernel(matrices, vectors, results):
    pos = cuda.grid(1)
    _vector_mul(matrices[pos], vectors[pos], results[pos])


def test_vector_mul():
    threadsperblock = 1024
    blockspergrid = 512
    num = threadsperblock * blockspergrid

    matrices = np.random.rand(num, 3, 3)
    vectors = np.random.rand(num, 3)
    p_results = np.zeros((num, 3))
    s_results = np.zeros((num, 3))

    vector_mul_kernel[blockspergrid, threadsperblock](
        matrices, vectors, p_results)

    for pos in range(num):
        s_results[pos] = np.dot(matrices[pos], vectors[pos])

    is_matrix_similar(s_results, p_results)


@cuda.jit
def matrix_mul_kernel(lefts, rights, results):
    pos = cuda.grid(1)
    _matrix_mul(lefts[pos], rights[pos], results[pos])


def test_matrix_mul():
    threadsperblock = 1024
    blockspergrid = 512
    num = threadsperblock * blockspergrid

    lefts = np.random.rand(num, 3, 3)
    rights = np.random.rand(num, 3, 3)
    p_results = np.zeros((num, 3, 3))
    s_results = np.zeros((num, 3, 3))

    matrix_mul_kernel[blockspergrid, threadsperblock](
        lefts, rights, p_results)

    for pos in range(num):
        s_results[pos] = np.dot(lefts[pos], rights[pos])

    is_matrix_similar(s_results, p_results)


@cuda.jit
def pose_mul_kernel(lefts, rights, results):
    pos = cuda.grid(1)
    _pose_mul(lefts[pos, :3, :3], lefts[pos, :3, 3],
              rights[pos, :3, :3], rights[pos, :3, 3],
              results[pos, :3, :3], results[pos, :3, 3])


def test_pose_mul():
    threadsperblock = 512
    blockspergrid = 512
    num = threadsperblock * blockspergrid

    lefts = np.array([np.identity(4)]*num)
    rights = np.array([np.identity(4)]*num)
    lefts[:, :3, :] = np.random.rand(num, 3, 4)
    rights[:, :3, :] = np.random.rand(num, 3, 4)
    p_results = np.array([np.identity(4)]*num)
    s_results = np.array([np.identity(4)]*num)

    pose_mul_kernel[blockspergrid, threadsperblock](
        lefts, rights, p_results)

    for pos in range(num):
        s_results[pos] = np.dot(lefts[pos], rights[pos])

    is_matrix_similar(s_results, p_results)


def test_pose_mul_on_self():
    threadsperblock = 512
    blockspergrid = 512
    num = threadsperblock * blockspergrid

    rights = np.array([np.identity(4)]*num)
    rights[:, :3, :] = np.random.rand(num, 3, 4)
    p_results = np.array([np.identity(4)]*num)
    p_results[:, :3, :] = np.random.rand(num, 3, 4)
    s_results = p_results[:, :, :]

    pose_mul_kernel[blockspergrid, threadsperblock](
        p_results, rights, p_results)

    for pos in range(num):
        s_results[pos] = np.dot(s_results[pos], rights[pos])

    is_matrix_similar(s_results, p_results)
